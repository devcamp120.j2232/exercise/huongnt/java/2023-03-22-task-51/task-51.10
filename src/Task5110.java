public class Task5110 {
    
    public static void main(String[] args) {
        //task1
        String[] inputs = {"Java", "Devcamp JAVA exercise", "Devcamp"};
        for (String input : inputs) {
            String result = Task1(input);
            System.out.println(result);
        }
        System.out.println("-----------------");

        //task2
        boolean isReverse1 = Task2("word", "drow");
        if (isReverse1) {
            System.out.println("OK");
        } else {
            System.out.println("KO");
        }

        boolean isReverse2 = Task2("java", "js");
        if (isReverse2) {
            System.out.println("OK");
        } else {
            System.out.println("KO");
        }

        System.out.println("-----------------");

        //task3
        String input1 = "Java";
        String input2 = "Haha";
        String input3 = "Devcamp";
        System.out.println(Task3(input1)); // output: J
        System.out.println(Task3(input2)); // output: NO
        System.out.println(Task3(input3)); // output: D

        System.out.println("-----------------");

        //task4
        String input41 = "word";
        String input42 = "java";
        
        String reverse41 = Task4(input41);
        String reverse42 = Task4(input42);
        
        System.out.println(reverse41);
        System.out.println(reverse42);

        System.out.println("-----------------");

        //task5
        String input51 = "abc";
        String input52 = "a1bc";
        System.out.println(Task5(input51));
        System.out.println(Task5(input52));

        System.out.println("-----------------");

        //task6
        String input61 = "java";
        int vowelCount61 = Task6(input61);
        System.out.println("Number of vowels in '" + input61 + "': " + vowelCount61);
        
        String input62 = "devcamp";
        int vowelCount62 = Task6(input62);
        System.out.println("Number of vowels in '" + input62 + "': " + vowelCount62);

        System.out.println("-----------------");

        //task7
        String input = "1234";
        int result = Task7(input);
        System.out.println(result);

        System.out.println("-----------------");

        //task8
        String str1 = "devcamp java";
        char[] fromChars1 = {'a', 'b'};
        char[] toChars1 = {'c', 'b'};
        String result1 = Task8(str1, fromChars1, toChars1);
        System.out.println(result1); // Devcbmp Jbvb

        String str2 = "exercise";
        char[] fromChars2 = {'e', 'f'};
        char[] toChars2 = {'x', 'r'};
        String result2 = Task8(str2, fromChars2, toChars2);
        System.out.println(result2); // fxfrcisf

        System.out.println("-----------------");

        //task9
        String input9 = "I am developer";
        String result9 = Task9(input9);
        System.out.println(result9);

        System.out.println("-----------------");

        //task10
        String str101 = "aba";
        String str102 = "abc";

        System.out.println(Task10(str101)); // true
        System.out.println(Task10(str102)); // false

         }

    public static String Task1(String input) {
        boolean hasDuplicate = false;
        int[] charCount = new int[256]; // Số ký tự ASCII
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c >= 'A' && c <= 'Z') {
                c += 32; // Chuyển đổi chữ hoa thành chữ thường
            }
            charCount[c]++;
        }
        String result = "";
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c >= 'A' && c <= 'Z') {
                c += 32; // Chuyển đổi chữ hoa thành chữ thường
            }
            if (charCount[c] > 1) {
                result += c;
                charCount[c] = 0; // Đánh dấu đã in ký tự này để tránh in trùng
                hasDuplicate = true;
            }
        }
        if (!hasDuplicate) {
            return "NO";
        } else {
            return result;
        }
    }


    public static boolean Task2(String str1, String str2) {
        boolean isReverse = true;
        if (str1.length() != str2.length()) {
            isReverse = false;
        } else {
            for (int i = 0; i < str1.length(); i++) {
                if (str1.charAt(i) != str2.charAt(str2.length() - 1 - i)) {
                    isReverse = false;
                    break;
                }
            }
        }
        return isReverse;
    }


    public static String Task3(String input) {
        String result = "NO";
        int n = input.length();
        int[] count = new int[256]; // 256 là số lượng ký tự ASCII
        for (int i = 0; i < n; i++) {
            char c = input.charAt(i);
            count[c]++;
        }
        for (int i = 0; i < n; i++) {
            char c = input.charAt(i);
            if (count[c] == 1) {
                result = Character.toString(c);
                break;
            }
        }
        return result;
    }

    public static String Task4(String input) {
        StringBuilder sb = new StringBuilder();
        for (int i = input.length() - 1; i >= 0; i--) {
            sb.append(input.charAt(i));
        }
        return sb.toString();
    }

    public static boolean Task5(String input) {
        for (int i = 0; i < input.length(); i++) {
            if (Character.isDigit(input.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static int Task6(String input) {
        int count = 0;
        char[] vowels = {'a', 'e', 'i', 'o', 'u'};
        for (char c : input.toCharArray()) {
            if (contains(vowels, c)) {
                count++;
            }
        }
        return count;
    }
    
    public static boolean contains(char[] arr, char c) {
        for (char element : arr) {
            if (element == c) {
                return true;
            }
        }
        return false;
    }

    public static int Task7(String str) {
        int result = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                throw new IllegalArgumentException("Input string contains non-numeric characters");
            }
            int digit = c - '0';
            result = result * 10 + digit;
        }
        return result;
    }

    public static String Task8(String str, char[] fromChars, char[] toChars) {
        if (fromChars.length != toChars.length) {
            throw new IllegalArgumentException("The length of fromChars and toChars must be equal.");
        }
        StringBuilder result = new StringBuilder(str);
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            for (int j = 0; j < fromChars.length; j++) {
                if (ch == fromChars[j]) {
                    result.setCharAt(i, toChars[j]);
                    break;
                }
            }
        }
        return result.toString();
    }

    public static String Task9(String input) {
        // Tách chuỗi thành các từ
        String[] words = input.split(" ");
    
        // Nếu chuỗi rỗng hoặc chỉ chứa một từ, trả về ngay lập tức
        if (words.length < 2) {
            return input;
        }
    
        // Duyệt qua các từ và nối chúng lại thành một chuỗi mới, đảo ngược thứ tự các từ
        StringBuilder sb = new StringBuilder();
        for (int i = words.length - 1; i >= 0; i--) {
            sb.append(words[i]);
            if (i > 0) {
                sb.append(" ");
            }
        }
    
        return sb.toString();
    }

    public static boolean Task10(String str) {
        String reverseStr = "";
        for (int i = str.length() - 1; i >= 0; i--) {
            reverseStr += str.charAt(i);
        }
        return str.equals(reverseStr);
    }

    }
    

